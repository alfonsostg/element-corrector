#!/usr/bin/python

import sys

def usage():
    print '+------------------------------------------------------------+'
    print '|            Negative Jacobian element rotator               |'
    print '|                         | BSC |                            |'
    print '| Usage:                                                     |'
    print '|       ./command input output                               |'
    print '|                                                            |'
    print '|To check that the software has worked OK just execute this  |'
    print '|script again to the modified file, and the message \"Some    |'
    print '|lines were modified!\" shuldn\'t appear.                      |'
    print '|Still not workin\'?! Write me a line:                        |'
    print '|Alfonso Santiago - alfonso.santiago@bsc.es                  |'
    print '|Ultra beta version.                                         |'
    print '+------------------------------------------------------------+'
    sys.exit()

#f = open("geometria.geo.dat.ORIG")
#fw = open ("geometria.geo.dat.MODIF", "w")


if(len(sys.argv)< 2):
    usage()
elif(sys.argv[1]=='-v'):
    usage()


f  = open(sys.argv[1], 'r')
fw = open(sys.argv[2], 'w')

FlagNodesPerElement=False
FlagElements=False
FlagCoordinates=False
FlagBoundaries=False
FlagSkewSystems=False
FlagModif=False

NumNodes=0
CoordNod=[]
EleNod=[]

gpcar=[[0.0 for x in xrange(3)]for x in xrange(3)]


print 'Reading the original file...'
#--------------------------INICIO ESCRITURA DEL ARCHIVO-------------------------------
for line in f:
    line = line.strip()
    if (line.startswith('END_')):
        FlagNodesPerElement=False
        FlagElements=False
        FlagCoordinates=False
        FlagBoundaries=False
        FlagSkewSystems=False

    elif (line.startswith('NODES_PER') or FlagNodesPerElement):
        FlagNodesPerElement=True

    elif (line.startswith('ELEMENTS') or FlagElements):
        FlagElements=True
        line=line.split()
        new=[]
        if line[0]!='ELEMENTS':
            new.append(int(line[1]))
            new.append(int(line[2]))
            new.append(int(line[3]))
            new.append(int(line[4]))
            EleNod.append(new)
           # print line[0]
           # print EleNod[int(line[0])-1]

    elif (line.startswith('COORDINATES') or FlagCoordinates):
        FlagCoordinates=True
        line=line.split()
        new=[]
        if line[0]!='COORDINATES':
            new.append(float(line[1]))
            new.append(float(line[2]))
            new.append(float(line[3]))
            CoordNod.append(new)
#            print line[0]
#            print CoordNod[int(line[0])-1]
#            print CoordNod[int(line[0])-1][0]

    elif (line.startswith('BOUNDARIES') or FlagBoundaries):
        FlagBoundaries=True

    elif (line.startswith('SKEW_') or FlagSkewSystems):
        FlagSkewSystems=True

    else:
       print 'La linea leida no esta entre las opciones'



#----------------------------FIN LECTURA DEL ARCHIVO----------------------------------
f.close()
f  = open(sys.argv[1], 'r')

print 'Writing the new file...'
#--------------------------INICIO ESCRITURA DEL ARCHIVO-------------------------------

FlagNodesPerElement=False
FlagElements=False
FlagCoordinates=False
FlagBoundaries=False
FlagSkewSystems=False

for line in f:
    line = line.strip()
    if (line.startswith('END_')):
        FlagNodesPerElement=False
        FlagElements=False
        FlagCoordinates=False
        FlagBoundaries=False
        FlagSkewSystems=False
        fw.write(line)
        fw.write('\n')

    elif (line.startswith('NODES_PER') or FlagNodesPerElement):
        FlagNodesPerElement=True
        fw.write(line)
        fw.write('\n')

    elif (line.startswith('ELEMENTS') or FlagElements):
        FlagElements=True
        line=line.split()

        if line[0]!='ELEMENTS':
            elem=int(line[0])
        #TODO rotar los ultimos dos numeros cuando el jacobiano sea negativo. Para calcular el jacobiano se usa el siguiente algoritmo (ESCRITO EN FORTRAN):
#                                      elcod(AXIS,  NODE)                   elcod(AXIS,  NODE)
#        gpcar[0][0]=CoordNod[EleNod[elem-1][NODE]][AXIS] - CoordNod[EleNod[elem][NODE]][AXIS]

#            print 'NUEVA LINEA:'
#            print line
#            print '-'
#            print elem
#            print '-'
#            print EleNod[elem-1][0]
#            print CoordNod[EleNod[elem-1][0]-1]
#            print '-'
#            print EleNod[elem-1][1]
#            print CoordNod[EleNod[elem-1][1]-1]
#            print '-'
#            print EleNod[elem-1][2]
#            print CoordNod[EleNod[elem-1][2]-1]
#            print '-'
#            print EleNod[elem-1][3]
#            print CoordNod[EleNod[elem-1][3]-1]

#            if (elem>424420):
#                print 'NUEVA LINEA'
#                print line
#                print elem
#                print EleNod[elem-1]
#                print CoordNod[EleNod[elem-1][1]-1]

#           gpcar(1, 1) =                  elcod(1,2) -                   elcod(1,1)
            gpcar[0][0]=CoordNod[EleNod[elem-1][1]-1][0] - CoordNod[EleNod[elem-1][0]-1][0]

#           gpcar(1, 2) =                  elcod(1,3) -                   elcod(1,1)
            gpcar[0][1]=CoordNod[EleNod[elem-1][2]-1][0] - CoordNod[EleNod[elem-1][0]-1][0]

#           gpcar(1 ,3) =                  elcod(1,4) -                   elcod(1,1)
            gpcar[0][2]=CoordNod[EleNod[elem-1][3]-1][0] - CoordNod[EleNod[elem-1][0]-1][0]

#           gpcar(2, 1) =                  elcod(2,2) -                   elcod(2,1)
            gpcar[1][0]=CoordNod[EleNod[elem-1][1]-1][1] - CoordNod[EleNod[elem-1][0]-1][1]

#           gpcar(2 ,2) =                  elcod(2,3) -                   elcod(2,1)
            gpcar[1][1]=CoordNod[EleNod[elem-1][2]-1][1] - CoordNod[EleNod[elem-1][0]-1][1]

#           gpcar(2 ,3) =                  elcod(2,4) -                   elcod(2,1)
            gpcar[1][2]=CoordNod[EleNod[elem-1][3]-1][1] - CoordNod[EleNod[elem-1][0]-1][1]

#           gpcar(3 ,1) =                   elcod(3,2) -                  elcod(3,1)
            gpcar[2][0]=CoordNod[EleNod[elem-1][1]-1][2] - CoordNod[EleNod[elem-1][0]-1][2]

#           gpcar(3 ,2) =                    elcod(3,3) -                  elcod(3,1)
            gpcar[2][1]=CoordNod[EleNod[elem-1][2]-1][2] - CoordNod[EleNod[elem-1][0]-1][2]

#           gpcar(3 ,3) =                   elcod(3,4) -                   elcod(3,1)
            gpcar[2][2]=CoordNod[EleNod[elem-1][3]-1][2] - CoordNod[EleNod[elem-1][0]-1][2]

#           t1         =  gpcar(2, 2) * gpcar(3, 3) - gpcar(3, 2) * gpcar(2, 3)
            t1         =  gpcar[1][1] * gpcar[2][2] - gpcar[2][1] * gpcar[1][2]

#           t2         = -gpcar(2 ,1) * gpcar(3, 3) + gpcar(3, 1) * gpcar(2, 3)
            t2         = -gpcar[1][0] * gpcar[2][2] + gpcar[2][0] * gpcar[1][2]

#           t3         =  gpcar(2, 1) * gpcar(3, 2) - gpcar(3, 1) * gpcar(2, 2)
            t3         =  gpcar[1][0] * gpcar[2][1] - gpcar[2][0] * gpcar[1][1]

#           gpdet      =  gpcar(1,1)  * t1 + gpcar(1,2)  * t2 + gpcar(1,3)  * t3
            gpdet      =  gpcar[0][0] * t1 + gpcar[0][1] * t2 + gpcar[0][2] * t3

#            print gpdet


            if (gpdet<=0.0):
                FlagModif=True
                fw.write(line[0])
                fw.write(" ")
                fw.write(line[1])
                fw.write(" ")
                fw.write(line[2])
                fw.write(" ")
                fw.write(line[4])
                fw.write(" ")
                fw.write(line[3])
                fw.write('\n')
            else:
                fw.write(line[0])
                fw.write(" ")
                fw.write(line[1])
                fw.write(" ")
                fw.write(line[2])
                fw.write(" ")
                fw.write(line[3])
                fw.write(" ")
                fw.write(line[4])
                fw.write('\n')
        else:
            fw.write(line[0])
            fw.write('\n')

    elif (line.startswith('COORDINATES') or FlagCoordinates):
        FlagCoordinates=True
        fw.write(line)
        fw.write('\n')
   
    elif (line.startswith('BOUNDARIES') or FlagBoundaries):
        FlagBoundaries=True
        fw.write(line)
        fw.write('\n')

    elif (line.startswith('SKEW_') or FlagSkewSystems):
        FlagSkewSystems=True
        fw.write(line)
        fw.write('\n')
    
    else:
       print 'La linea leida no esta entre las opciones'

if (FlagModif):
    print 'Some lines were modified!' 
else:
    print 'No line was modified.'

print 'Bye! :)'

f.close()
fw.close()
